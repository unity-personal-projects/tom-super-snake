from tkinter import *
from tkinter import filedialog
import random
from Snake import *
from Fruit import *


class Board(Canvas):
    def __init__(self, frame):
        super().__init__(frame, width=520, height=520,
                         background="#121", highlightthickness=0)

        self.start()
        self.pack()

    def start(self):

        self.playing = True
        self.score = 0
        self.lives = 3

        self.snake = Snake()
        self.fruit = Fruit(self.snake)

        self.bind_all("<Key>", self.onKeyPressed)
        self.after(100, self.update)

    def update(self):
        if self.playing:
            print("playing")
            self.delete("all")

            self.snake.move()
            self.playing = not (self.collisionSnakeWalls()
                                or self.collisionSnakeTail())
            if (self.playing):
                self.collisionSnakeFruit()

                self.drawFruit()
                self.drawSnake()
                self.drawBorders()
                self.drawScore()
                self.drawLives()

            self.after(100, self.update)
        else:
            print("not playing")
            self.delete("all")
            self.drawGameOver()

    def drawSnake(self):
        drawColor = ""
        for i in range(0, len(self.snake.posX)):
            if (i == 0):
                drawColor = "#1F1"
            elif (i % 2 == 0):
                drawColor = "#1A5"
            else:
                drawColor = "#15A"
            self.create_oval(
                self.snake.posX[i],
                self.snake.posY[i],
                self.snake.posX[i] + self.snake.size,
                self.snake.posY[i] + self.snake.size,
                outline=drawColor,
                fill=drawColor
            )

    def drawScore(self):
        self.create_text(50, 10,
                         text="Score : " + str(self.score), fill="white")

    def drawLives(self):
        self.create_text(450, 10,
                         text="Vies : " + str(self.lives), fill="white")

    def drawGameOver(self):
        self.create_text(250, 220, text="Game Over !", fill="white")
        self.create_text(250, 250, text="Score : " +
                         str(self.score), fill="white")
        self.create_text(
            250, 280, text="Appuyez sur <espace> pour recommencer", fill="white")

    def drawFruit(self):
        self.create_oval(
            self.fruit.posX,
            self.fruit.posY,
            self.fruit.posX + self.fruit.size,
            self.fruit.posY + self.fruit.size,
            outline="#F00",
            fill="#F00"
        )

    def drawBorders(self):
        self.create_rectangle(0, 0, 20, 520, outline='#833', fill='#833')
        self.create_rectangle(0, 0, 520, 20, outline='#833', fill='#833')
        self.create_rectangle(500, 0, 520, 520, outline='#833', fill='#833')
        self.create_rectangle(0, 500, 520, 520, outline='#833', fill='#833')

    def collisionSnakeFruit(self):
        if (self.snake.posX[0] == self.fruit.posX and self.snake.posY[0] == self.fruit.posY):
            self.fruit = Fruit(self.snake)
            self.snake.posX.append(self.snake.newTailPosX)
            self.snake.posY.append(self.snake.newTailPosY)
            self.score += 100

    def collisionSnakeWalls(self):
        return self.snake.posX[0] <= 0 or self.snake.posX[0] >= 500 or self.snake.posY[0] <= 0 or self.snake.posY[0] >= 500

    def collisionSnakeTail(self):
        if (len(self.snake.posX) > 1):
            for i in range(1, len(self.snake.posX)):
                if (self.snake.posX[0] == self.snake.posX[i] and self.snake.posY[0] == self.snake.posY[i]):
                    self.lives -= 1
        return self.lives <= 0

    def onKeyPressed(self, event):
        key = event.keysym
        if key == "Left" and self.snake.speedX <= 0:
            self.snake.speedX = -self.snake.SPEED
            self.snake.speedY = 0
        if key == "Right" and self.snake.speedX >= 0:
            self.snake.speedX = self.snake.SPEED
            self.snake.speedY = 0
        if key == "Up" and self.snake.speedY <= 0:
            self.snake.speedX = 0
            self.snake.speedY = -self.snake.SPEED
        if key == "Down" and self.snake.speedY >= 0:
            self.snake.speedX = 0
            self.snake.speedY = self.snake.SPEED

        if key == "space" and not self.playing:
            self.playing = True
            self.start()
