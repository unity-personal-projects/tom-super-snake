from tkinter import *
from tkinter import filedialog
import os
import time
from Board import *


class SnakeWindow(Tk):
    """
    Classe permettant de créer une fenètre de notre application. Cette classe s'occupe de toute la partie interfaces.
    """

    def __init__(self):
        """
        Constructeur configurant la fenètre suivant nos exigences : 
        - Impossible de la resize
        """
        super(SnakeWindow, self).__init__()
        self.title("Alex Super Snake")
        self.resizable(width=False, height=False)

        self.frame = Frame()
        self.canvas = Board(self.frame)
        self.frame.pack()

