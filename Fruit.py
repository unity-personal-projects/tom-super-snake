import random

class Fruit:
    def __init__(self, snake):
        self.size = 20
        relaunch = True
        while relaunch:
            self.posX = random.randint(1, (500 - 20) / 20) * 20
            self.posY = random.randint(1, (500 - 20) / 20) * 20
            relaunch = self.fruitInSnake(snake)
    
    def fruitInSnake(self, snake):
        for i in range(0, len(snake.posX)):
            if (self.posX == snake.posX[i] and self.posY == snake.posY[i]):
                return True
        return False
