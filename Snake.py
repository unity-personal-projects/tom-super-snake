
class Snake:
    def __init__(self):
        self.size = 20
        self.posX = [240]
        self.posY = [240]
        self.SPEED = 20
        self.speedX = 0
        self.speedY = 0
        self.newTailPosX = 240
        self.newTailPosY = 240

    def move(self):
        previousXPos = 0
        previousYPos = 0
        self.newTailPosX = self.posX[len(self.posX) - 1]
        self.newTailPosY = self.posY[len(self.posY) - 1]
        for i in range(0, len(self.posX)):
            tempX = self.posX[i]
            tempY = self.posY[i]
            if (i == 0):
                self.posX[i] += self.speedX
                self.posY[i] += self.speedY
            else:
                self.posX[i] = previousXPos
                self.posY[i] = previousYPos
            previousXPos = tempX
            previousYPos = tempY
